package main

import (
	"database/sql"
	"log"
	"net/http"
	"os"

	"bitbucket.org/xudejian/story"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
)

func main() {
	db, err := sql.Open("mysql", "root:@/blog?parseTime=true")
	if err != nil {
		log.Fatalln("connect db fail", err)
	}
	defer db.Close()

	sess_store := sessions.NewCookieStore([]byte("secret"))
	sess_name := "gss"

	opt := story.Options{
		Tmpl:  "./templates",
		Confd: "./",
	}
	s := story.NewStory(db, nil, opt)
	s.Session(sess_name, sess_store)

	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "3000"
	}

	host := os.Getenv("HOST")
	http.Handle("/", s)
	log.Fatal(http.ListenAndServe(host+":"+port, nil))
}
